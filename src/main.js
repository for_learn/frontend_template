import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import VueCookies from 'vue-cookies'
import axios from 'axios' //追記
import VueAxios from 'vue-axios' //追記
Vue.use(VueCookies)
Vue.use(VueAxios, axios) //追記
Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
